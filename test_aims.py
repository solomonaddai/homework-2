import aims
from nose.tools import asser_equal

def test_pos_ints():
    obs = aims.std([2,4,4,5,4,5])
    exp=1.0954
    assert_equal(obs,exp)

def test_pos_ints():
    obs = aims.std([2,4,4,5,4,5])
    exp=1.0954
    assert_almost_equal(obs,exp)

def test_pos_ints():
    obs = aims.avg_range([2,4,4,5,4,5])
    exp=1.0954
    assert_equal(obs,exp)

def test_pos_ints():
    obs = aims.avg_range([2,4,4,5,4,5])
    exp=1.0954
    assert_almost_equal(obs,exp)




